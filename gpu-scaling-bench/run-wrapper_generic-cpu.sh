#!/bin/bash

# Sample generic run script for CPU runs to measure nonbonded
# kernel performance.
#
# TODO: CPU model detection not supported, always have to pass DEVNAME!
#
# NOTE: in order to able to get the correct Nonbonded kernel, a
# custom patch is necessary that splits the buffer zerioing and
# nonbonded OpenMP loop

module load gromacs/2016

#NSTL_VALUES="10 20 40"
NSTL_VAL="20"

export INPUT_DIRS="0000.96 0001.5 0003 0006 0012 0024 0048 0096 0192 0384 0768 1536 3072"
export KTIME_PREC=4
export CPU_RUN=1

gmx=$(which gmx)
gmx=${GMX:-$gmx}

########################################################################
# run CPU-only mode to measure nonbonded performance
function bench_nonbonded_cpu_example
{
  module rm gcc
  module load gcc/X.Y
  for nstl in $NSTL_VAL; do

    export GMX_NSTLIST=$nstl
    export PER1K=1 NVPROF=1
    export DEVNAME=i7-4960X
    export OMP_NUM_THREADS=16
    LOGSUFFIX=nstl${nstl}_GMX16_${OMP_NUM_THREADS}T_gccYY MDRUN=$gmx \
      ./run-water-bench-tune.sh
  done
  module rm gcc
}
########################################################################

