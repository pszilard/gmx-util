#!/bin/bash

PROGNAME=$(basename $0)

# $1 - env var
# $2 - help text
function print_help_line
{ 
  printf "\t%-17s%s\n" "$1" "$2"
}

if [[ "$1" == "-h" || "$1" == "--help" ]]; then
  echo    "Controlling the script is done through the following environment variables:"
  print_help_line "CPU_RUN" "Switches to running CPU-only benchmarks; note that for accurate measurements you need tweaked nonbonded kernel code that splits out the force buffer zeroing. If this is not done set the  PARSE_COMB_CPU_NB_TIME to still use the timing data"
  print_help_line "MDRUN" "the gmx or mdrun binary to benchmark"
  print_help_line "OMP_NUM_THREADS" "Number of OpenMP threads to use (default 0 => auto)"
  print_help_line "NRANKS" "Number of ranks to use (default 1)"
  print_help_line "RUNTUNE" "if set, PP-PME tuning will be left enabled, otherwise runs will keep the fixed cut-off"
  print_help_line "INPUT_DIRS" "a space-separated list of input directories"
  print_help_line "INPUT_TPR" "name of the tpr's to benchmark (in each input dir)"
  print_help_line "LOGSUFFIX" "suffix to append to log and result output files"
  print_help_line "GMX_GPU_ID" "ID of the GPU to benchmark; note that the indexing here matched the runtime's indexing which may not be identical with the system indexing (e.g. nvidia-smi)"
  print_help_line "DEVNAME" "the name of the device to be used in the log and output files; useful with OpenCL or when the CUDA runtime messes up the GPU oder and GMX_GPU_ID does not match the nvidia-smi order"
  print_help_line "NVPROF" "if set the benchmarks will be run in nvprof and its output will be parsed"
  print_help_line "PARSE_COMB_CPU_NB_TIME" "Even if the nonbonded compute and force buffer zeroing are not split up, parse the combined number."
  print_help_line "GMX_NSTLIST" "nstlist parameter"
  print_help_line "FIXED_STEPS" "the number of steps to run the benchmarks (default 1000 only for nvprof runs, ignored with tunning runs)\t"
  print_help_line "STEPFAC" "Used as a multiplier when taking the number of steps encoded in the tpr input"
  print_help_line "FORCE_FULL_INFO" "Always parse all data columns (also CPU force time/step, ns/day, ratio)"
  print_help_line "NVPROF_TIME_UNIT" "Time unit of the kernel timings to pass to nvprof"
  print_help_line "KTIME_PREC" "Floating point precision to use when printing kernel times; often 4 digits are necessary to avoid rounding in ms."
  exit 0;
fi

# deprecated options: PER1K
[ -n "${PER1K}" ] && { echo -e "\n\n>>>> WARNING: PER1K not supported!\n\n"; }

# Function for exit due to fatal program error
# $1  exit code
# $2  string containing descriptive error message
function error_exit
{
  echo
  echo "${PROGNAME}: ${@:-"unknown error"}" 1>&2
  exit 1
}

###############################################################
#### Setting defaults: variables, env vars, programs, etc. ####
###############################################################
export GMX_MAXBACKUP=-1
#export GMX_DETAILED_PERF_STATS=1
export GMX_CUDA_STREAMSYNC=1

cpu_only_run=0
if [ -n "${CPU_RUN}" ]; then
  cpu_only_run=1
  measure_nb_gpu=0;  measure_nb_cpu=1;
  measure_pme_gpu=0; measure_pme_cpu=1;
elif [ -n "${PME_CPU_RUN+x}" ]; then
  measure_nb_gpu=1;  measure_nb_cpu=0;
  measure_pme_gpu=0; measure_pme_cpu=1;
  [ -n "${PRINT_PME_CPU}" ] && print_pme_cpu=1
else
  measure_nb_gpu=1;  measure_nb_cpu=0;
  measure_pme_gpu=1; measure_pme_cpu=0;
fi

if [ -n "${BONDED_GPU_RUN+x}" ]; then
  measure_bonded_gpu=1;
else
  measure_bonded_gpu=0
fi

dirs="[0-9]*"
#dirs="0003 0006"
#dirs=0*
dirs=${INPUT_DIRS:-$dirs}

[ -n "${VERBOSE+x}" ] && dump_output="1" || dump_output="0"
# if set only parses log files and generates summary table
[ -n "${ONLY_PARSE+x}" ] && only_parse=1 || only_parse=0

# default binary names
mdrun="mdrun"
gmxdump="gmxdump"
mdrun=${MDRUN:-$mdrun}
gmxdump=${GMXDUMP:-$gmxdump}
[ -z "${NVPROF_TIME_UNIT}" ] && nvprof_time_unit=ms || nvprof_time_unit=$NVPROF_TIME_UNIT
nvprof_args="$NVPROF_EXTRA_ARGS -u $nvprof_time_unit --csv"

# we assume here that with GROMACS 5.0 we use the gmx binary
if [[ $(basename $mdrun) = gmx* ]]; then
  use_gmx=1
  gmx=$mdrun
fi

input_tpr="topol.tpr"
input_tpr=${INPUT_TPR:-$input_tpr}

run_opts="-quiet -resethway -noconfout -pin on"
[ "$measure_nb_cpu" -gt 0 ] && run_opts="$run_opts -nb cpu" || run_opts="$run_opts -nb gpu"
[ "$measure_pme_cpu" -gt 0 ] && run_opts="$run_opts -pme cpu" || run_opts="$run_opts -pme gpu"

[ -n "$CPU_RUN" ] && stepfac=2 || stepfac=5 
stepfac=${STEPFAC:-$stepfac}
parse_comb_cpu_nb_time=0
parse_comb_cpu_nb_time=${PARSE_COMB_CPU_NB_TIME:-$parse_comb_cpu_nb_time}

nsteps_min=200 #2000
nsteps_min=${NSTEPS_MIN:-$nsteps_min}
# when profiling we use a reasonably short fixed step-count run
prof_fixed_steps=1000
prof_fixed_steps=${FIXED_STEPS:-$prof_fixed_steps}

if [ -n "${RUNTUNE+x}" ]; then
do_tune=1
run_opts="$run_opts -tunepme"
else
do_tune=0
run_opts="$run_opts -notunepme"
fi

ktime_prec=4
ktime_prec=${KTIME_PREC:-$ktime_prec}
ktime_fmt="10.${ktime_prec}f"
other_perf_fmt="10.3f"
###############################################################


# Returns 1 if the version string indicates GROAMCS 5.0 or newer (including the 2016 bs),
# 0 otherwise (note the swap!).
#
# $1 - version string
function is_gmx_v5_or_newer
{
  if [[ $1 =~ ^5\.[01]  || $1 =~ ^20[12][0-9] ]]; then echo 1; else echo 0; fi;
}

# Returns 1 if the version string indicates GROAMCS 2020 or newer,
# 0 otherwise (note the swap!).
#
# $1 - version string
function is_gmx_v2020_or_newer
{
  if [[ $1 =~ ^20[2][0-9] ]]; then echo 1; else echo 0; fi;
}

# Returns the GROMACS version string parsed out from the output of the gmx/mdrun -version output
# or empty string if the binary does not work.
#
# $1 - gmx/mdrun binary
function get_gmx_version_string
{
  local verstr=""
  local binary=$1
  [ ! -x $binary ] && return;

  verstr=$($binary --version 2>&1 | egrep "GROMACS version|Gromacs version" | awk '{print $NF}')

  echo -n $verstr
}

# $1 - string
# $2 - field separator
# $3 - field number
function parse_field_from_str
{
  if [ -z "$1" ]; then
    echo "0.0"
  else
    echo "$1" | \
      awk -v field="$3" -F "$2" \
      '{if (NF < field) {print "0.0"} else {print $field}}END{if(NR<1) print "0.0"}'
  fi
}

# $1 - nvprof binary
# returns: the nvprof major version
function parse_nvprof_major_version
{
  [ $# -lt 1 ] && return;
  local nvprof_bin=$1
  echo $($nvprof_bin --version | grep 'Release version' | awk -v ver=9 '/Release version [0-9]/{ print int($3) }')
}

# Convert elapsed wall time in seconds to ms/step
# $1  wall time in seconds
# $2  number of steps
# returns string containing $1/$2 with 8 digits prec
function convert_to_ms_per_step
{
  [ $# -lt 2 ] && { echo 0; return; }
  awk -v walltime=$1 -v nsteps=$2 'BEGIN{printf("%.8f", 1000*(walltime/nsteps))}'
}

# Calculate the wall-time of an mdrun performance log entry in ms/step
# It only assumes that the log file passed contains rows with total time in the
# second to last column and the number of calls the third to last column,
# so it should work with both CPU perf table as well as GPU table.
#
# $1 - mdrun log file path
# $2 - regex string to parse the performance table row
# $3 - number of steps to divide by
# returns the time/step with (8 digits prec) corresponding to the timed task passed from the mdrun log
function get_mdrun_log_entry_in_ms_per_step
{
  local _mdlog=$1
  local _regexstr=$2
  local _nsteps=$3

  [ $# -lt 2 -o ! -f $_mdlog ] && { echo 0; return; }

  local log_row=$(tail -n70 $1 | grep "$_regexstr")
  [ -z "${log_row}" ] && { echo 0; return; }

  if [ -z "$_nsteps" ]; then
    convert_to_ms_per_step $(echo $log_row | awk '{print $(NF-2), $(NF-3)}')
  else
    convert_to_ms_per_step $(echo $log_row | awk -v nsteps=${_nsteps} '{print $(NF-2), nsteps}')
  fi
}

# Calculate performance per 1k atoms
# $1 - performance number
# $2 - number of atoms
# returns $1/$2 (with 8 digits prec)
function calc_per1k
{
  [ $# -lt 2 ] && { echo 0; return; }

  awk -v sys_size=$2 -v val=$1 'BEGIN{printf("%.8f",val/sys_size)}'
}

# Parse and calculate the total time/step based on the total execution time reported at the end of the log
# $1 - mdrun log file path
# returns the total time/step (with 8 digits prec)
function get_mdrun_log_total_cpu_time_per_step
{
  [ $# -lt 1 -o ! -f $1 ] && { echo 0; return; }
  local mdlog=$1
  local nsteps=$(grep " Update " $mdlog | awk '{print $4}')
  [ -z "$nsteps" ] && { echo 0; return; }
  echo $( convert_to_ms_per_step "$(tail -n20 $mdlog | grep "       Time:" | awk '{print $3}')" "$nsteps" )
}

# $1 - log output file
# $2 - profiler output file
# $3 - mdrun version string
function parse_kernel_perf
{
  local mdlog=$1
  local proflog=$2
  local gmx_version=$3

  local _gpu_k_f _gpu_k_fe _gpu_k_fp _gpu_k_fep _cpu_t _ns_day _ratio
  ((_gpu_k_f=_gpu_k_fe=_gpu_k_fp=_gpu_k_fep=_cpu_t=_ns_day=_ratio=0))

  local _gpu_pme_spread _gpu_pme_solve _gpu_pme_solve_enervir _gpu_pme_gather _gpu_pme_fft
  ((_gpu_pme_spread=_gpu_pme_solve=_gpu_pme_solve_enervir=_gpu_pme_gather=_gpu_pme_fft=0))

  local _gpu_bonded
  ((_gpu_bonded=0))
 
  _ratio="1 $mdlog"

  if [ -z "$NVPROF" ]; then
    # NB
    _gpu_k_f=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "Nonbonded F kernel ")
    _gpu_k_fe=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "Nonbonded F+ene ")
    _gpu_k_fp=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "Nonbonded F+prune ")
    _gpu_k_fep=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "Nonbonded F+ene+prun e")
    # FIXME: should use ms/step rather than per call?
    _gpu_k_prune=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "Pruning kernel ")
    _gpu_k_dyn_prune=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "Dynamic pruning ")

    # PME
    _gpu_pme_spread=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "PME spline + spread ")
    _gpu_pme_gather=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "PME gather ")
    _gpu_pme_solve=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "PME solve ")
    ((_gpu_pme_fft_r2c=_gpu_pme_fft_c2r=0))
    _gpu_pme_fft_r2c=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "PME 3D-FFT r2c ")
    _gpu_pme_fft_c2r=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "PME 3D-FFT c2r ")
    _gpu_pme_fft=$(echo "$_gpu_pme_fft_r2c" "$_gpu_pme_fft_c2r" | awk '{print $1 + $2}')

    # no GPU bonded data in the log
    
  else
    # kernels got renamed in 5.0
    if [ $(is_gmx_v5_or_newer $gmx_version) -eq 1 ]; then
      local kernel_name='nbnxn_kernel'
      local Eflag="VF"
      local pruneonly="kernel_prune_cuda"
    else
      local kernel_name='k_nbnxn'
      local Eflag="ener"
    fi
    # alnternatively we could use the column count if this does not work somehwere:
    # if [ $(grep $kernel_name | head -n1 | awk -F ',' '{print NF}') -gt 10 ]; then k_avg_field=5 else k_avg_field=4; fi
    [[ $(parse_nvprof_major_version nvprof) -lt 9 ]] && k_avg_field=4 || k_avg_field=5
    #[[ $(nvprof --version | grep version) =~ 11. ]] && k_avg_field=5 || k_avg_field=4
    _gpu_k_f=$(parse_field_from_str  "`cat $proflog  | grep $kernel_name | grep -v $Eflag | grep -v prune | grep -v "$pruneonly"`" ',' "$k_avg_field" )
    _gpu_k_fe=$(parse_field_from_str "`cat $proflog  | grep $kernel_name | grep    $Eflag | grep -v prune | grep -v "$pruneonly"`" ',' "$k_avg_field" )
    _gpu_k_fp=$(parse_field_from_str "`cat $proflog  | grep $kernel_name | grep -v $Eflag | grep    prune | grep -v "$pruneonly"`" ',' "$k_avg_field" )
    _gpu_k_fep=$(parse_field_from_str "`cat $proflog | grep $kernel_name | grep    $Eflag | grep    prune | grep -v "$pruneonly"`" ',' "$k_avg_field" )
    _gpu_k_prune=$(parse_field_from_str "`cat $proflog | grep $kernel_name     | grep $pruneonly | grep '<bool=1>'`" ',' "$k_avg_field" )
    _gpu_k_dyn_prune=$(parse_field_from_str "`cat $proflog | grep $kernel_name | grep $pruneonly | grep '<bool=0>'`" ',' "$k_avg_field" )

    # PME
    kernel_name='pme_'
    _gpu_pme_spread=$(parse_field_from_str  "`cat $proflog  | grep $kernel_name | grep spread `" ',' "$k_avg_field" )
    _gpu_pme_gather=$(parse_field_from_str  "`cat $proflog  | grep $kernel_name | grep gather `" ',' "$k_avg_field" )
    _gpu_pme_solve=$(parse_field_from_str  "`cat $proflog  | grep $kernel_name | grep solve  | grep bool=0 `" ',' "$k_avg_field" )
    _gpu_pme_solve_enervir=$(parse_field_from_str  "`cat $proflog  | grep $kernel_name | grep solve  | grep bool=1 `" ',' "$k_avg_field" )
    k_call_count_field=$(($k_avg_field - 1))
    _gpu_pme_fft=`cat $proflog | grep -i 'radix\|fft\|tmpxft\|bluestein' | awk -v field=$k_avg_field -v callfield=$k_call_count_field -F ',' 'nsteps=999999999 {if ($callfield < nsteps) nsteps=$callfield fi; SUM += $callfield * $field} END {print SUM/nsteps}'`  
    # multiple cuFFT kernels launched different number of times per each step; we sum total kernel time and divide it by minimum call count

    kernel_name='::exec_kernel_gpu'
    _gpu_bonded_f=$(parse_field_from_str  "`cat $proflog  | grep $kernel_name | grep '<bool=0, bool=0>' `" ',' "$k_avg_field" )
    # note that this is only the virial+energy kernel
    _gpu_bonded_fe=$(parse_field_from_str  "`cat $proflog  | grep $kernel_name | grep '<bool=1, bool=1>' `" ',' "$k_avg_field" )

  fi

  _cpu_t=$(get_mdrun_log_total_cpu_time_per_step $mdlog)
  _ns_day=`tail -n10 $mdlog | grep Performance | awk '{if (NF < 4) {printf("%s", $2)} else {printf("%s", $4)}}'`
  _ratio=`tail -n25 $mdlog | grep "Force GPU/CPU evaluation" | awk '{print $NF}'`

  echo "${_gpu_k_f},${_gpu_k_fe},${_gpu_k_fp},${_gpu_k_fep},${_gpu_k_prune},${_gpu_k_dyn_prune},${_gpu_pme_spread},${_gpu_pme_gather},${_gpu_pme_fft},${_gpu_pme_solve},${_gpu_pme_solve_enervir},${_gpu_bonded_f},${_gpu_bonded_fe},${_cpu_t},${_ns_day},${_ratio}"
}

# $1 - log output file
# $2 - gmx version string
function parse_cpu_nb_perf
{
  local mdlog=$1
  local gmx_version=$2

  local cpu_nb_k cpu_nb_clear cpu_nb_prune cpu_t ns_day
  ((cpu_nb_k=cpu_nb_clear=cpu_nb_prune=cpu_t=ns_day=0))

  local nsteps=$(grep " Update " $mdlog | awk '{print $4}')

  # convert to ms
  if [[ $parse_comb_cpu_nb_time == 0 ]]; then
    # before 2020 subcounters were not upstream and were extras with different names
    if [ $(is_gmx_v2020_or_newer $gmx_version) -eq 1 ]; then
      f_kernel_str="Nonbonded F kernel"
      clear_kernel_str="Nonbonded F clear"
      prune_kernel_str="Nonbonded pruning"
    else
      f_kernel_str="NB F kernel "
      clear_kernel_str="NB F clear"
      prune_kernel_str="Nonbonded pruning"
    fi
    cpu_nb_k=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "$f_kernel_str")
    cpu_nb_clear=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "$clear_kernel_str")
    cpu_nb_prune=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "$prune_kernel_str" "$nsteps")
  else
    # parse output of binary that does not report separate NB / clear time
    cpu_nb_k=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "Nonbonded F  ")
  fi

  cpu_t=$(get_mdrun_log_total_cpu_time_per_step $mdlog)

  ns_day=$(tail -n10 $mdlog | grep Performance | awk '{if (NF < 4) {printf("%s", $2)} else {printf("%s", $4)}}')

  echo "${cpu_nb_k},${cpu_nb_clear},${cpu_nb_prune},${cpu_t},${ns_day}"
}

# $1 - log output file
function parse_cpu_pme_perf
{
  local mdlog=$1

  local cpu_pme_spread cpu_pme_solve cpu_pme_gather cpu_pme_fft cpu_t ns_day
  ((cpu_pme_spread=cpu_pme_solve=cpu_pme_gather=cpu_pme_fft=cpu_t=ns_day=0))

  local nsteps=$(grep " Update " $mdlog | awk '{print $4}')
  # convert to ms
  cpu_pme_spread=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "PME spread ")
  cpu_pme_gather=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "PME gather ")
  cpu_pme_solve=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "PME solve Elec ")
  cpu_pme_fft=$(get_mdrun_log_entry_in_ms_per_step "$mdlog" "PME 3D-FFT  ")

  # TODO: here we exclude "PME 3D-FFT comm", but should we do that?

  cpu_t=$(get_mdrun_log_total_cpu_time_per_step $mdlog)
  ns_day=$(tail -n10 $mdlog | grep Performance | awk '{if (NF < 4) {printf("%s", $2)} else {printf("%s", $4)}}')

  echo "${cpu_pme_spread},${cpu_pme_gather},${cpu_pme_solve},${cpu_pme_fft}"
}

# Checks and returns the system size either from the input name or tpr
#
# If reading from the tpr, we assume that we want the size in thousands of atoms.
# $1 - input name
# $2 - tpr
# $3 - gmx dump command
function get_system_size
{
  local input_name=$1
  local tpr=$2
  local gmxdump=$3

  case $input_name in
    ''|*[!0-9]*) local name_is_number=0 ;;
    *)           local name_is_number=1 ;;
  esac

  if [[ $name_is_number == 0 || -n "${PARSE_SIZE_FROM_TPR}" ]]; then
    local size=$( $gmxdump -s $tpr 2>&1 | grep -i natoms | awk '{printf("%.3f", $NF/1000.0)}' )
  else
    local size=$input_name
  fi

  echo $size
}

# Print information header
# $1 - mdrun/gmx binary
# $2 - the gmx/mdrun version string
# $3 - number of ranks used
# $4 - number of threads used
# $5 - per1k output file
function print_info_header
{
  local mdrun=$1
  local gmx_ver_str=$2
  local nrank=$3
  local nth=$4
  local outf_per1k=$5

  # full -version output to parse more info
  local gmx_ver_out="$($mdrun -version 2>&1)"

  if [ $do_tune -eq 1 ]; then
    echo -e -n "\n`date`\nRunning tuned water scaling benchmark on ${nrank}x${nth} cores using:\n"
  else
    echo -e -n "\n`date`\nRunning un-tuned water scaling benchmark on ${nrank}x${nth} cores using:\n"
  fi

  local c_compiler="$(echo "${gmx_ver_out}" | grep "C compiler:" | cut -d ' ' -f3- | xargs)"
  local c_compiler_flags="$(echo "${gmx_ver_out}" | grep "C compiler flags:" | cut -d ' ' -f4- | sed 's/-W[^ ]*//g' | xargs)"
  local cpp_compiler="$(echo "${gmx_ver_out}" | grep "C++ compiler:" | cut -d ' ' -f3- | xargs)"
  local cpp_compiler_flags="$(echo "${gmx_ver_out}" | grep "C++ compiler flags:" | cut -d ' ' -f4- | sed 's/-W[^ ]*//g' | xargs)"
  local cuda_compiler="$(echo "${gmx_ver_out}" | grep "CUDA compiler:" | awk '{print $3, $(NF-1), $NF}'| xargs)"
  local cuda_compiler_flags=$(echo "${gmx_ver_out}" | grep "CUDA compiler flags:" | sed 's/CUDA compiler flags:[ ]*//' | sed 's/-W[^;]*//g' | sed 's/-Xcompiler;[^;].*//' | tr -s ';,')

  cw=17
  printf  "\t %-${cw}s%s\n" "host:"           "${HOSTNAME}"
  printf  "\t %-${cw}s%s\n" "mdrun:"          "${mdrun}"
  printf  "\t %-${cw}s%s\n" "version:"        "${gmx_ver_str}"
  printf  "\t %-${cw}s%s\n" "C compiler:"     "${c_compiler}"
  printf  "\t %-${cw}s%s\n" "C flags:"        "${c_compiler_flags}"
  printf  "\t %-${cw}s%s\n" "C++ compiler:"   "${cpp_compiler}"
  printf  "\t %-${cw}s%s\n" "C++ flags:"      "${cpp_compiler_flags}"
  printf  "\t %-${cw}s%s\n" "CUDA compiler:"  "${cuda_compiler}"
  printf  "\t %-${cw}s%s\n" "CUDA flags:"     "${cuda_compiler_flags}"

  echo -e -n "Timings collected from "
  [ -n "$NVPROF" ] && echo -e "nvprof output\n" || echo -e "mdrun timing output\n"

  if [ "$cpu_only_run" -eq 0 ]; then
      out=$(printf "%-13s%-10s%-10s%-10s%-10s%-10s%-10s" "# Sys. name " "GPU F" "GPU Fe" "GPU Fp" "GPU Fep" "GPU prune" "GPU dynpr";
	  [ $measure_pme_gpu -gt 0 ] && printf "%-10s%-10s%-10s%-10s%-10s" "Spread" "Gather" "FFT" "Solve" "Solve_En"; 
	  [ $measure_pme_cpu -gt 0 ] && printf "%-8s%-10s%-10s%-10s" "Spread" "Gather" "FFT" "Solve";
          [ $measure_bonded_gpu -gt 0 ] && printf "%-10s%-10s" "Bonded F" "Bonded FE";
	  [ $do_tune -eq 1  -o "$FORCE_FULL_INFO" ] && printf "%-10s%-10s%15s" "CPU tot. " "ns/day " "Ratio (GPU/CPU-overlap)";
	  printf "\n";)
      echo "$out"
  else
      out=$(printf "%-13s%-10s%-10s%-10s" "# Sys. name " "CPU NB F" "CPU clear" "CPU prune";
	  [ $measure_pme_cpu -gt 0 ] && printf "%-10s%-10s%-10s%-10s" "Spread" "Gather" "FFT" "Solve";
	  printf "%-10s%-10s\n" "CPU tot." "ns/day ";
	  printf "\n";)
      echo "$out"
  fi

}

# $1 - mdrun 
# $3 - number of ranks
# $4 - number of threads
# $5 - mdrun log file suffix
# $6 - output file for per 1000 results
function run_it 
{
  #echo 1=$1 2=$2 3=$3 4=$4 5=$5; exit 0
  local _mdrun___NOT_USED="$1"
  local _np=$2
  local _nt=$3
  local suffix="$4"
  local outf_per1k="$PWD/$5"

  if [[ -n "$use_gmx" ]]; then
    local _mdrun="$gmx mdrun"
    local _gmxdump="$gmx dump"
  else
    local _mdrun="$mdrun"
    local _gmxdump="$gmxdump"
  fi

  #local gmx_ver_out=$(tempfile)
  #${_mdrun} -version > $gmx_ver_out 2>&1
  #local mdrun_version=`cat $gmx_ver_out | egrep "GROMACS version|Gromacs version" | awk '{print $NF}'`

  local gmx_ver_str=$(get_gmx_version_string $_mdrun)

  # print header
  print_info_header "${_mdrun}" "${gmx_ver_str}" "${_np}" "${_nt}" "${outf_per1k}"

  perfout="perf_${suffix}.out"
  mdlog="md_${suffix}.log"
  proflog="prof_$mdlog"

  # Stupidly enough, with GROMACS 5.0 the GMX_NSTLIST env var has been dropped,
  # so we need to make sure we don't get hit by the nstlist auto-tuning.
  if [ $(is_gmx_v5_or_newer $gmx_ver_str) -eq 1 ]; then
    [ -n "$GMX_NSTLIST" ] && extra_opts=" -nstlist $GMX_NSTLIST"
  fi

  # pick up extra mdrun options passed from outside 
  extra_opts="$extra_opts $EXTRA_OPTS"

  # NVIDIA profiler command line
  [ -n "$NVPROF" ] && nvprof_cmd="nvprof $nvprof_args --log-file $proflog"

  # Loop over the directories and run the benchmark in each
  starting_dir=$PWD
  for f in $dirs; do
    cd $f
    #### in the target dir now ####

    sys_size=$(get_system_size "$f" "$input_tpr" "$_gmxdump")

    if [ -n "$DO_COOLDOWN_PAUSE" -a $( echo "$sys_size >= 384" | bc) -eq 1 ]; then
      [ $DO_COOLDOWN_PAUSE -gt 1 ] && sleep $DO_COOLDOWN_PAUSE || sleep 1m
    fi

    # Run fixed number of steps when parsing results from profiler output
    # OR when the FIXED_STEPS env var is defined.
    #if [ -n "$NVPROF" -o -n "$FIXED_STEPS" -a $do_tune -ne 1 ]; then
    if [[ -z "$DISABLE_SHORT_RUN"  && ( -n "$NVPROF" || -n "$FIXED_STEPS" && $do_tune -ne 1 ) ]]; then
      nsteps=$prof_fixed_steps
    else
      nsteps_tpr=`$_gmxdump -s $input_tpr 2>&1 | grep nsteps | awk '{print $3}'`
      nsteps=$(awk -v nst=$nsteps_tpr -v fac=$stepfac 'BEGIN{printf("%.0f\n", (nst*fac))}')
      #[ $do_tune -eq 1 -a $nsteps -lt $nsteps_min ] && nsteps=$nsteps_min
      [ $nsteps -lt $nsteps_min ] && nsteps=$nsteps_min
    fi

    # run simulation
    if [ $only_parse -eq 0 ]; then
      [ $dump_output -eq 1 ] && echo "RUNNING: $nvprof_cmd $_mdrun -ntmpi $_np -ntomp $_nt $run_opts $extra_opts -nsteps $nsteps -s $input_tpr -g $mdlog 2>&1"
      stdout=`$nvprof_cmd $_mdrun -ntmpi $_np -ntomp $_nt $run_opts $extra_opts -nsteps $nsteps -s $input_tpr -g $mdlog 2>&1`
      [ $? -ne 0 ] && echo -e "=== Simulation failed! ==="
      [ $dump_output -eq 1 ] && echo "$stdout"
    fi

    # check for missing/invalid results
    # TODO
    warn=""
    if [[ -f "$mdlog" && $(grep "Can not increase nstlist" $mdlog -c) -gt 0 ]]; then
      warn="${warn} FAIL: could not increase nstlist;"
    else
      ##################
      ## parse results ##
      local gpu_k_f gpu_k_fe gpu_k_fp gpu_k_fep cpu_t ns_day ratio
      # PME GPU kernels; gpu_pme_fft is special - (inaccurate) sum of many different cufft/clfft kernels
      local gpu_pme_spread gpu_pme_solve gpu_pme_solve_enervir gpu_pme_gather gpu_pme_fft

      ## NB/PME CPU run
      if [ $measure_nb_cpu -gt 0 ]; then
        ((cpu_k=cpu_clear=cpu_prune=cpu_t=ns_day=0))
        local _res=$(parse_cpu_nb_perf "${mdlog}" "$gmx_ver_str")
        IFS=',' read cpu_k cpu_clear cpu_prune cpu_t ns_day <<< "$_res"
      fi

      if [ $measure_pme_cpu -gt 0 ]; then
        ((cpu_pme_spread=cpu_pme_gather=cpu_pme_solve=cpu_pme_fft=0))
        local _res=$(parse_cpu_pme_perf "${mdlog}")
        IFS=',' read cpu_pme_spread cpu_pme_gather cpu_pme_solve cpu_pme_fft <<< "$_res"
      fi

      ## GPU run
      [ ! $measure_nb_cpu -gt 0 ]  && ((gpu_k_f=gpu_k_fe=gpu_k_fp=gpu_k_fep=gpu_k_prune=gpu_k_dynprune=cpu_t=ns_day=ratio=0))
      [ ! $measure_pme_cpu -gt 0 ] && ((gpu_pme_spread=gpu_pme_solve=gpu_pme_solve_enervir=gpu_pme_gather=gpu_pme_fft=0))
      ((gpu_bonded_f=gpu_bonded_fe=0))

      if [ $measure_nb_gpu -gt 0 -o $measure_pme_gpu -gt 0 ]; then
        local _res=$(parse_kernel_perf "${mdlog}" "$proflog" "$gmx_ver_str")
        IFS=',' read gpu_k_f gpu_k_fe gpu_k_fp gpu_k_fep gpu_k_prune gpu_k_dynprune gpu_pme_spread gpu_pme_gather gpu_pme_fft gpu_pme_solve gpu_pme_solve_enervir gpu_bonded_f gpu_bonded_fe cpu_t ns_day ratio <<< "$_res"
      fi
    fi

    ######
    # CPU-only perf print
    if [ $cpu_only_run -eq 1 ]; then
      # System size
      printf "%-13s" "$f"
      # NB data
      printf "%-${ktime_fmt}%-${ktime_fmt}%-${ktime_fmt}" "$cpu_k" "$cpu_clear" "$cpu_prune"
      # PME data
      if [ $measure_pme_cpu -gt 0 ]; then
          printf "%-${ktime_fmt}%-${ktime_fmt}%-${ktime_fmt}%-${ktime_fmt}" "$cpu_pme_spread" "$cpu_pme_gather" "$cpu_pme_fft" "$cpu_pme_solve" 
      fi	  
      # Overall performance	
      printf "%-${ktime_fmt}%-${other_perf_fmt}\t${warn}\n" "$cpu_t" "$ns_day"
    else 
      # GPU-only perf print
      # TODO: fix mixed mode!

      local all_columns_fmt_str="%-13s%-${ktime_fmt}%-${ktime_fmt}%-${ktime_fmt}%-${ktime_fmt}%-${ktime_fmt}%-${ktime_fmt}"
      [ "$measure_pme_gpu" -gt 0 ] &&\
        local pme_all_columns_fmt_str="%-${ktime_fmt}%-${ktime_fmt}%-${ktime_fmt}%-${ktime_fmt}%-${ktime_fmt}"
      [ "$measure_bonded_gpu" -gt 0 ] &&\
        local bonded_all_columns_fmt_str="%-${ktime_fmt}%-${ktime_fmt}"
      local full_info_format_str="%-${other_perf_fmt}%-${ktime_fmt}%-15s"

      printf "${all_columns_fmt_str}"\
        "$f" "$gpu_k_f" "$gpu_k_fe" "$gpu_k_fp" "$gpu_k_fep" "$gpu_k_prune" "$gpu_k_dynprune"
      if [ $measure_pme_gpu -gt 0 ]; then
        printf "${pme_all_columns_fmt_str}"\
          "$gpu_pme_spread" "$gpu_pme_gather" "$gpu_pme_fft" "$gpu_pme_solve" "$gpu_pme_solve_enervir"
      elif [ $measure_pme_cpu -gt 0 ]; then
        printf "${bonded_all_columns_fmt_str}"\
          "$cpu_pme_spread" "$cpu_pme_gather" "$cpu_pme_fft" "$cpu_pme_solve"
      fi
      if [ $measure_bonded_gpu -gt 0 ]; then
        printf "${bonded_all_columns_fmt_str}"\
          "$gpu_bonded_f" "$gpu_bonded_fe"
      fi
      [ $do_tune -eq 1 -o "$FORCE_FULL_INFO" ] && printf "${full_info_format_str}" "$cpu_t" "$ns_day" "$ratio"
      printf "\t${warn}\n"
    fi

    cd $starting_dir
  done

  echo -e "\n`date`"
}

# Check whether a binary is accessible and runs.
# $1 - binary 
# $2 - prog name
function check_prog 
{
  bin=$1
  prog=$2
  
  [[ ! "which $bin" ]] && error_exit "the binary for $prog is not accessible"
  
  $bin -h 1>/dev/null 2>&1 || error_exit "the binary \"$bin\" doesn't seem to work!"
}

#######################
# Check prerequisites #
#######################

## The binaries we need
if [[ $(is_gmx_v5_or_newer `get_gmx_version_string $mdrun`) -eq 1 && "`basename $mdrun`" == gmx* ]]; then
  check_prog "$gmx" "gmx"
else
  check_prog $mdrun "mdrun"
  check_prog $gmxdump "gmxdump"
fi
[ -n "$NVPROF" ] && check_prog "nvprof" "nvprof"
[ `$mdrun -version 2>&1 | grep "GPU support" | grep disabled -c` -ne 0 ] && error_exit "$mdrun is compiled without GPU support!"
##

## GPU name qurying function
# $1 - GPU id 
function query_devname 
{
  nvidia-smi -q -g $1 | grep "Product Name" | \
    awk -F ":" '{print $2}' | \
    sed 's/ GeForce //' | sed 's/ Tesla //' | sed 's/[ ]//g'
}

## Runner
# $1 GPUname
# $2 Nranks
# $3 NT
# $4 prefix
# $5 logsuffix
function run_bench
{
  local gpuname=$1
  local nrank=$2
  local nth=$3
  local prefix=$4
  local logsuff=$5

  if [ $do_tune -eq 1 ]; then
    local s="${nrank}x${nth}_${gpuname}_tuned"
  else
    local s="${gpuname}"
  fi

  local suffix="${s}${logsuff}"
  local outf=${prefix}_${suffix}
  [ $only_parse -eq 1 ] && outf=${outf}_parsed

  run_it $mdrun $nrank $nth $suffix | tee $outf
}


####################################################################################
# TODO
# - test mdrun timing, nvprof timing +/- tuning
####################################################################################
# initiating the runs below 

## Filename components: profiled runs get a "p" prefix for their logs
# FIXME: buggy condition!
[ -n "$NVPROF" ] && default_prefix=bench || default_prefix=pbench
default_logsuff=${LOGSUFFIX:-$default_logsuff}
[ -n "${default_logsuff}" ] && default_logsuff="_${default_logsuff}"

nstlist=20
nstlist=${NSTLIST:-$nstlist}
[ -z "$GMX_NSTLIST" ] && export GMX_NSTLIST=$nstlist

##############################
# Set up a generic run
##############################
GENERIC=1 # XXX
if [ -n "$GENERIC" ]; then
  gpuid=0
  gpuid=${GMX_GPU_ID:-$gpuid}

  export LC_NUMERIC="en_US.UTF-8"

  # FIXME: this orginal conditional doesn't look right
  #if [ -z "$CPU_RUN" -o  -z "$PME_CPU_RUN" ]; then
  if [ -z "$CPU_RUN" ]; then
    if [ -z "$GMX_GPU_ID" ]; then
      echo "No GPU set, using GPU #0!"
      export GMX_GPU_ID=0
    fi
  else
    [ -z "$DEVNAME" ] && { echo "error: must set DEVNAME, there is no CPU detection!"; exit 1; } ||\
     echo "CPU run on $DEVNAME"
  fi

  module rm gromacs
  [ -z "$DEVNAME" ] && gpu_name=`query_devname $gpuid` || gpu_name=$DEVNAME
  [ -z "$gpu_name" ] && error_exit "Could not get the GPU name!" || echo "Using GPU: $gpu_name"
  module load gromacs

  nt=0 # let mdrun decide the #threads unless OMP_NUM_THREADS is defined
  nt=${OMP_NUM_THREADS:-$nt}
  nr=1
  np=${NRANKS:-$nr}
  run_bench "$gpu_name" "$nr" "$nt" "$default_prefix" "$default_logsuff"
fi

exit 0


####################################################################################
# The configs below are examples of manual possible setups specific to use-cases
####################################################################################

# use nvprof measurements
# run 8 CPU threads, use GPU 0 and pass the GPU name manually
MDRRUN=/my/path/to/gmx OMP_NUM_THREADS=8 \
  GMX_GPU_ID=0 DEVNAME=TITANXp \
  NVPROF=1 LOGSUFFIX=mytest_8T \
  ./run-water-bench-tune.sh

# use log-file measurements (e.g. for OpenCL or if nvprof can't be used)
# use default (detected) thread count and GPU name detection
MDRRUN=/my/path/to/gmx LOGSUFFIX=mytest_auto \
  ./run-water-bench-tune.sh

# For OpenCL if can be handy to gen/reuse the cache e.g. by doing a short manual run
# and copying/symlinking the generated cache file into the run dirs.
# BEWARE that consistency is not checked
# NOTE: device name detection does not work with OpenCL!
GMX_OCL_GENCACHE=1 MDRUN=/path/to/gmx2016/bin/gmx \
   GMX_GPU_ID=0 DEVNAME=FuryN \
   LOGSUFFIX=gmx16_ocl \
  ./run-water-bench-tune.sh
