#!/bin/bash

# Sample generic run script to measure CUDA and OpenCL
# nonbonded kernel performance

module load gromacs/2016

#NSTL_VALUES="10 20 40"
NSTL_VAL="20"

export INPUT_DIRS="0000.96 0001.5 0003 0006 0012 0024 0048 0096 0192 0384 0768 1536 3072"
export KTIME_PREC=4

gmx=$(which gmx)
gmx=${GMX:-$gmx}

########################################################################
# run with CUDA 8.0 and use nvprof
function bench_cuda_nonbonded_example
{
  module load cuda/8.0
  for nstl in $NSTL_VAL; do
    export GMX_NSTLIST=$nstl
    export PER1K=1 NVPROF=1
    export GMX_GPU_ID=0
    LOGSUFFIX=nstl${nstl}_GMX16_cu80 MDRUN=$gmx ./run-water-bench-tune.sh
  done
  module rm cuda/8.0
}
########################################################################


########################################################################
# run with OpenCL so use log file
# parse OpenCL driver version from clinfo output

function bench_ocl_nonbonded_example
{
  local drv_version=$(clinfo | grep -i "driver.*(vm)" | sed 's/.* \([0-9].*\) (VM).*/\1/')
  echo "Detected OpenCL driver version: $drv_version"
  export SUFF="_${drv_version}"

  for nstl in $NSTL_VAL; do
    export GMX_NSTLIST=$nstl
    export PER1K=1
    export GMX_GPU_ID=0 DEVNAME=FuryN
    LOGSUFFIX=nstl${nstl}_GMX16_OCL${SUFF} MDRUN=$gmx ./run-water-bench-tune.sh
  done
}
########################################################################


########################################################################
# Run with BOTH nvprof and mdrun GPU timing disabled; this is useful to
# measure launch overhead of CUDA/OpenCL APIs.
function bench_gpu_api_launchpverhead_example
{
  for nstl in $NSTL_VAL; do
    export GMX_NSTLIST=$nstl
    export GMX_DISABLE_GPU_TIMING=1 # GMX_DISABLE_CUDA_TIMING=1 GMX_DISABLE_OCL_TIMING=1
    export XSUFF=_API-overhead

    export OMP_NUM_THREADS=8
    export NRANKS=1
    export GMX_GPU_ID=0
    LOGSUFFIX=nstl${nstl}_GMX16_${NRANKS}x${OMP_NUM_THREADS}T$XSUFF} MDRUN=$gmx \
      ./run-water-bench-tune.sh
  done
}
########################################################################



