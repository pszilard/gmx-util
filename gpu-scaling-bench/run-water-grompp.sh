#!/bin/bash

PROGNAME=$(basename $0)

# Function for exit due to fatal program error
# $1  exit code
# $2  string containing descriptive error message
function error_exit
{
  echo
  echo "${PROGNAME}: ${@:-"unknown error"}" 1>&2
  exit 1
}

dirs="[0-9\.]*"
mdp="pme.mdp"
grompp="grompp"
prefix="."

[ -n "${VERBOSE+x}" ] && dump_output="1" || dump_output="0"

# take defaults if not defined 
mdp=${MDP:-$mdp}
grompp=${GROMPP:-$grompp}
prefix=${PREFIX:-$prefix}

[ $# -gt 0 ] && dirs="$@"

[ ! -x "`which $grompp`" ] &&  error_exit "$grompp is not accesible"
[ ! -d "$prefix" ] && error_exit "prefix directory \"$prefix\" is not accesible"

currdir=`pwd`
echo "Running $grompp the direcotry $prefix using $mdp:"
for d in $prefix/$dirs; do
  echo -n "$d ... "
  cd $d
  #output=`$grompp -f $mdp >/dev/null 2>&1`
  output=`$grompp -f $mdp 2>&1`
  [ $? -eq 0 ] && echo "done." || echo "FAILED!"
  [ $dump_output -eq 1 ] && echo "$output"
  cd $currdir
done
