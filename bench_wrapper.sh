# common options
module load cuda/9.0
module load gcc/5.4
module load fftw
module load hwloc

export MDRUN=/nethome/yupinov/gromacs/build-release-cuda-9.0/bin/gmx
bench_script="/nethome/yupinov/gmx-util-pme/gpu-scaling-bench/run-water-bench-tune.sh"
#export INPUT_DIRS="0000.96 0001.5 0003 0006 0012 0024 0048 0096 0192 0384"
export INPUT_DIRS="0000.96 0001.5" #for quick testing


# kernel measurements with nvprof:
#if [ ]; then

export LOGSUFFIX="kernels"
export GMX_GPU_ID=0 
#export DEVNAME=GTX750 #in case name is not detected automatically
export GMX_DISABLE_GPU_TIMING=1
export NVPROF_EXTRA_ARGS='--unified-memory-profiling off --concurrent-kernels off --profile-from-start off'
export NVPROF=1 
export FIXED_STEPS=2000
$bench_script

#fi

